Depot::Application.routes.draw do
  resources :users
  resources :products do
    get :who_bought, on: :member
  end

  get 'admin' => 'admin#index'
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  scope '(:locale)' do
    resources :orders
    resources :line_items
    resources :carts
    root to: 'store#index', as: 'store', via: :all
  end
end
